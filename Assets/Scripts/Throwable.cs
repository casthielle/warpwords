﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour {

    [Tooltip("Velocidad de movimiento en unidades del mundo")]
    public float speed;
    
    public float factor;

    GameObject player;
    Rigidbody2D rb2d;
    Vector3 target, direction;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player");
        rb2d = GetComponent<Rigidbody2D>();

        if(player != null) {
            target = player.transform.position;
            direction = (target - transform.position).normalized;
        }    
    }

    void FixedUpdate() {
        if(target != Vector3.zero) {
            rb2d.MovePosition(transform.position + (direction * speed) * Time.deltaTime);
        } 
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if(collider.transform.tag == "Player" || collider.transform.tag == "Attack") {
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
