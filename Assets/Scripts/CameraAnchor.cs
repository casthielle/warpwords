﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnchor : MonoBehaviour {

    public Transform target;
    public float delay = 0.05f;
    float minX, maxY, maxX, minY;
    GameObject map;
    Vector2 mapConfig;
    Vector3 mapPosition;
    Vector3 lastPosition;

    void Start() {
        map = GameObject.FindGameObjectWithTag("Map");
        mapConfig = map.GetComponent<SpriteRenderer>().sprite.rect.size;
        mapPosition = map.transform.position;
        if(target == null) target = GameObject.FindGameObjectWithTag("Player").transform;
        SetBound();
    }

    void FixedUpdate() {
        if(target != null) {
            lastPosition = new Vector3(
                Mathf.Clamp(target.position.x, minX, maxX),
                Mathf.Clamp(target.position.y + 1.5f, minY, maxY),
                transform.position.z
            );
            transform.position = Vector3.Lerp(transform.position, lastPosition, delay);
        }
    }

    public void SetTarget(Transform transform){
        target = transform;
    }

    public void SetBound() {
        float cameraSize = Camera.main.orthographicSize;
        minX = (mapPosition.x - (mapConfig.x / 2)) + (cameraSize * 1.78f);
        maxX = (mapPosition.x + (mapConfig.x / 2)) - (cameraSize * 1.78f);
        minY = (mapPosition.y - (mapConfig.y / 2)) + cameraSize;
        maxY = (mapPosition.y + (mapConfig.y / 2)) - cameraSize;
    }
}
