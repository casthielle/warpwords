﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour {

    public string animation = "Destroy";
    public float disabledTime = 0.2f;
    Animator animator;

    void Start() {
        animator = GetComponent<Animator>();
    }

    IEnumerator OnTriggerEnter2D(Collider2D collider) {
        if(collider.tag == "Attack") {
            animator.Play(animation);
            yield return new WaitForSeconds(disabledTime);
            foreach(Collider2D c in GetComponents<Collider2D>()) {
                c.enabled = false;
            }
        }
    }

    void Update() {
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if(stateInfo.IsName(animation) && stateInfo.normalizedTime >= 1) {
            Destroy(gameObject);
        }
    }
}
