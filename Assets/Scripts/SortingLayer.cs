﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayer : MonoBehaviour {

    SpriteRenderer sprite;
    public bool active = false;

    void Start() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sortingLayerName = "Player";
        sprite.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100);
    }

    void Update() {
        if(transform.CompareTag("Player") || active) sprite.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100);
    }
}
