﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Warp : MonoBehaviour {

    public GameObject target;
    public GameObject map;
    bool start = false;
    bool isFadeIn = false;
    float alpha = 0;
    float fadeTime = 1f;
    GameObject camera;

    void Awake() {
        Assert.IsNotNull(target);
        Assert.IsNotNull(map);
        GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        camera = GameObject.FindGameObjectWithTag("CameraAnchor");
    }

    /* IEnumerator OnTriggerEnter2D(Collider2D collider) {
        float defaultDelay = camera.GetComponent<CameraAnchor>().delay;
        if(collider.tag == "Player") {
            collider.GetComponent<Animator>().enabled = false;
            collider.GetComponent<Player>().enabled = false;
            FadeIn();
            yield return new WaitForSeconds(fadeTime);
            camera.GetComponent<CameraAnchor>().delay=1f;
            collider.transform.position = target.transform.GetChild(0).transform.position;
            yield return new WaitForSeconds(fadeTime);
            camera.GetComponent<CameraAnchor>().delay=defaultDelay;
            FadeOut();
            collider.GetComponent<Animator>().enabled = true;
            collider.GetComponent<Player>().enabled = true;
        }
    }*/

    void OnGUI() {
        if(!start)
            return;
        
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        Texture2D texture;
        texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, Color.black);
        texture.Apply();
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);

        if(isFadeIn) {
            alpha = Mathf.Lerp(alpha, 1.1f, fadeTime * Time.deltaTime);
        } else {
            alpha = Mathf.Lerp(alpha, -0.1f, fadeTime * Time.deltaTime);
            if(alpha < 0)
                start = false;
        }
    }

    void FadeIn() {
        start = true;
        isFadeIn = true;
    }

    void FadeOut() {
        isFadeIn = false;
    }
}
