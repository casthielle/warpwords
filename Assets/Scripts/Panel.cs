﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{
    public string side;
    bool open;
    Animator animator;
    GameObject mainUI;
    // Start is called before the first frame update
    void Start() {
        animator = GetComponent<Animator>();
    }

    public void toggle() {
        if(side == "left"){
          animator.SetBool("left", true);
        } else {
          animator.SetBool("left", false);
        }
        open = animator.GetBool("open");
        animator.SetBool("open", !open);
        transform.Find("Close").gameObject.SetActive(!open);
    }
}
