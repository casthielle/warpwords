﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour {

    public float spawnArea;
    public float maxObjects;
    public float spawnDelay;
    public bool showSpawner;
    public GameObject objectToSpawner;

    float timer;
    float spawnedObjects = 0f;
    GameObject player;
    GameObject camera;
    List<string> resources;

    void Awake() {
        if(!showSpawner) GetComponent<SpriteRenderer>().enabled = false;
    }

    void Start() {
        resources = new List<string>();
        player = GameObject.FindGameObjectWithTag("Player");
        camera = GameObject.FindGameObjectWithTag("CameraAnchor");
        resources.Add("EnemyResource");
    }

    void Update() {
        RaycastHit2D hit = Physics2D.Raycast(
            transform.position,
            player.transform.position - transform.position,
            spawnArea,
            1 << LayerMask.NameToLayer("Default")
        );
        Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
        if(hit.collider != null && hit.collider.tag == "Player") {
            if(spawnedObjects < maxObjects || maxObjects == 0) {
                timer += Time.deltaTime;
                if(timer >= spawnDelay) {
                    timer = 0;
                    float x = Random.Range(transform.position.x - spawnArea , transform.position.x + spawnArea);
                    float y = Random.Range(transform.position.y - spawnArea , transform.position.y + spawnArea);
                    Vector3 position = new Vector3(x, y, 0);
                    Quaternion rotation = new Quaternion();
                    Instantiate(objectToSpawner, position, rotation);
                    spawnedObjects++;
                } 
            }
        }
    }

    void FixedUpdate() {
        float cameraSize = Camera.main.orthographicSize;
        float right = camera.transform.position.x + (cameraSize * 1.78f);
        float left = camera.transform.position.x - (cameraSize * 1.78f);
        float up = camera.transform.position.y + (cameraSize / 2);
        float down = camera.transform.position.y - (cameraSize / 2);

        bool checkRight = transform.position.x < right;
        bool checkLeft = transform.position.x > left;
        bool checkUp = transform.position.y < up;
        bool checkDown = transform.position.y > down;

        if (!(checkRight && checkLeft && checkUp && checkDown)) {
            spawnedObjects = 0f;
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(transform.position, spawnArea);
    }

}
