﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float visionArea;
    public float attackArea;
    public float speed;
    public float attackFrecuency;
    public GameObject rock;
    public float hp;
    public float currentHp;
    bool attacking;

    GameObject player;
    Vector3 initialPosition, target;
    Animator animator;
    Rigidbody2D rb2d;
    List<string> resources;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player");
        initialPosition = transform.position;
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();  
        resources = new List<string>();
        resources.Add("RockResource");
        resources.Add("LeavesResource");
        currentHp = hp;
    }

    void Update() {
        target = initialPosition;
        RaycastHit2D hit = Physics2D.Raycast(
            transform.position,
            player.transform.position - transform.position,
            visionArea,
            1 << LayerMask.NameToLayer("Default")
        );

        Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
        Debug.DrawRay(transform.position, forward, Color.red);

        if(hit.collider != null && hit.collider.tag == "Player") {
            target = player.transform.position;
        }

        float distance = Vector3.Distance(target, transform.position);
        Vector3 direction = (target - transform.position).normalized;

        if(target != initialPosition && distance < attackArea) {
            animator.SetFloat("movX", direction.x);
            animator.SetFloat("movY", direction.y);
            animator.Play("EnemyWalk", -1, 0);

            if(!attacking) StartCoroutine(Attack(attackFrecuency));
        } else {
            rb2d.MovePosition(transform.position + direction * speed * Time.deltaTime);
            animator.speed = 1;
            animator.SetFloat("movX", direction.x);
            animator.SetFloat("movY", direction.y);
            animator.SetBool("walking", true);
        }

        if(target == initialPosition && distance < 1f) {
            transform.position = initialPosition;
            animator.SetBool("walking", false);
        }

        Debug.DrawLine(transform.position, target, Color.green);
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionArea);
        Gizmos.DrawWireSphere(transform.position, attackArea);
    }

    IEnumerator Attack(float seconds) {
        int index = UnityEngine.Random.Range(0, 2);
        GameObject resource = (GameObject)Resources.Load(resources[index], typeof(GameObject));
        attacking = true;
        if(target != initialPosition){
            Instantiate(resource, transform.position, transform.rotation);
            yield return new WaitForSeconds(seconds);
        }
        attacking = false;
    }

    public void Attacked() {
        if(--currentHp <= 0) Destroy(gameObject);
    }

    void OnGUI() {
        Vector2 position = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Box(
            new Rect(
                position.x - 20,
                Screen.height - position.y + 20,
                40,
                24
            ), currentHp + "/" + hp
        );
    }
}
